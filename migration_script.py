import cx_Oracle
from config import ds3_userdata, ds3_appdata, pinpoint, gateway, ds3_userdata2, gateway2, ds3_appdata2
from Oracle_interface import OracleInterface

gw1 = OracleInterface(gateway)  # Instantiate Oracleinterface instance for your schema
gw1.connect()

app1 = OracleInterface(ds3_appdata)
app1.connect()
#app2 = OracleInterface(ds3_appdata2)
#app2.connect()

usr1 = OracleInterface(ds3_userdata)
usr1.connect()
#usr2 = OracleInterface(ds3_userdata2)
#usr2.connect()

#print(app1.sandbox('select * from projects_extras where project_ID in (21000,44001, 45001 )'))

# Migrating projects#
########################################################################################################################
#print('''
#Migrating Projects
#''')
#
#gw1.migrate('PROJECT_LIST', gateway2, 'PROJECT_LIST',
#            headers=['ID',
#                     'PROJECT',
#                     'DATAGATEWAY',
#                     'DESCRIPTION',
#                     'PREFIX',
#                     'BGCOLOR',
#                     'DISPLAY_ORDER',
#                     'GATEWAY_EXT',
#                     'ARCHIVED'],
#            conditions=['ID in (21000, 44001, 45001)'])
#
#app1.migrate('PROJECTS', ds3_appdata2, 'PROJECTS',
#            headers=['PROJECT_ID',
#                     'PROJECT_TYPE',
#                     'DESCRIPTION',
#                     'CACHE_STATUS',
#                     'CACHE_PROGRESS',
#                     'LAST_CACHE_UPDATE',
#                     'CACHE_JOB_ID',
#                     'REBUILD',
#                     'PRIORITY',
#                     'POP_INIT',
#                     'FOLDER_NAME'],
#            conditions=['PROJECT_ID in (21000, 44001, 45001)'])
#
#app1.migrate('PROJECTS_EXTRAS', ds3_appdata2, 'PROJECTS_EXTRAS',
#            headers=['PROJECT_ID',
#                     'RESULT_THROTTLE',
#                     'USE_CACHE',
#                     'CUSTOM_IMG_SIZE',
#                     'EDITABLE',
#                     'DYNAMIC_STRUCTURES',
#                     'ORDERBY',
#                     '"EXCLUSIVE"',
#                     'RUN_AFTER_QUERY',
#                     'EXCEL_IMG_SIZE',
#                     'AUTORUN',
#                     'HIDDEN',
#                     'ORDERBYBOX'],
#            conditions=['PROJECT_ID in (21000, 44001, 45001)'])
#
## Migrating forms#
#
#########################################################################################################################
#print('''
#Migrating Forms
#''')
#
#app1.migrate('PROJECT_FORMS', ds3_appdata2, 'PROJECT_FORMS',# '--verbose',
#             headers=['PROJECT_ID',
#                      'FORM_ID',
#                      'FORM_TYPE',
#                      'PARENT_PROJECT_ID',#
#                      'PARENT_FORM_ID',
#                      'NAME',
#                      'CURRENT_VERSION',
#                      'SCREEN_WIDTH',
#                      'SCREEN_HEIGHT'
#                      ],
#             conditions=['PROJECT_ID in (21000, 44001, 45001)'])
#
#app1.migrate('FORM_OBJECTS', ds3_appdata2, 'FORM_OBJECTS',# '--verbose',
#             headers=['PROJECT_ID',
#                      'FORM_ID',
#                      'FID',
#                      'ELEMENT_ID',
#                      'PARENT_PROJECT_ID',
#                      'PARENT_FORM_ID',
#                      'PARENT_FID'
#                      ],
#             conditions = ['PROJECT_ID in (21000, 44001, 45001)'])
#
#
#app1.migrate('F_O_ATTRIBUTES', ds3_appdata2, 'F_O_ATTRIBUTES',# '--verbose',
#             headers=['PROJECT_ID',
#                      'FORM_ID',
#                      'FID',
#                      'ELEMENT_ID',
#                      'NAME',
#                      'VALUE',
#                      'ORD'
#                      ],
#             conditions = ['PROJECT_ID in (21000, 44001, 45001)'])
##########################################################################################################################
#print('''
#Migrating Datasources
#''')
#app1.migrate('DATA_SOURCES', ds3_appdata2, 'DATA_SOURCES',# '--verbose',
#             headers=['PROJECT_ID',
#                      'DS_ID',
#                      'NAME',
#                      'TABLE_NAME',
#                      'JOIN_COLUMN',
#                      'DS_TYPE',
#                      'DATE_COLUMN',
#                      'ORDER_BY',
#                      'STATUS',
#                      'MESSAGE',
#                      'CONNECTION',
#                      'PIVOT_ID',
#                      'BLOB_OPTIONS',
#                      'SUMMARY_JOIN',
#                      'MORE_JOINS'
#                      ],
#             conditions = ['PROJECT_ID in (21000, 44001, 45001)'])
#
#app1.migrate('ALL_COLUMN_DICT', ds3_appdata2, 'ALL_COLUMN_DICT',# '--verbose',
#             headers=['PROJECT_ID',
#                      'DS_TYPE',
#                      'JOIN_COLUMN',
#                      'DS_NAME',
#                      'DS_ID',
#                      'ORDER_BY',
#                      'NAME',
#                      'COLUMN_ID',
#                      'ALIAS_NAME',
#                      'COLOUR',
#                      'COLUMN_NAME',
#                      'DATA_TYPE',
#                      'BROWSE_COLUMN_NAME',
#                      'COL_ORDER_BY',
#                      'CSS_ID',
#                      'PIVOT_ID'
#                      ],
#             conditions = ['PROJECT_ID in (21000, 44001, 45001)'])
#
#app1.migrate('COLUMN_ALIAS_DICT', ds3_appdata2, 'COLUMN_ALIAS_DICT',# '--verbose',
#             headers=['TABLE_NAME',
#                      'COLUMN_NAME',
#                      'ALIAS_NAME',
#                      'PROJECT'
#                      ],
#             conditions = ["TABLE_NAME = 'TM_EXPERIMENTS'", "column_name = 'DESCR'"])
########################################################################################################################
#print('''
#Migrating Column lookups and alters
#''')
#app1.migrate('COLUMN_LOOKUP_DICT', ds3_appdata2, 'COLUMN_LOOKUP_DICT',# '--verbose',
#             headers=['DS_ID',
#                      'COLUMN_NAME',
#                      'LOOKUP_SQL',
#                      'PROJECT_ID',
#                      'TEXT_SELECT',
#                      'OPTIONAL_DEFAULT',
#                      'SEARCH_FIRST',
#                      'GLOBAL_COLUMN'
#                      ],
#             conditions = ["DS_ID in (select DS_ID from data_sources where project_id in (21000, 44001, 45001))"])
#
#app1.migrate('COLUMN_ALTER_DICT', ds3_appdata2, 'COLUMN_ALTER_DICT',# '--verbose',
#             headers=['DS_ID',
#                      'COLUMN_NAME',
#                      'ACTION_ID',
#                      'PROJECT_ID',
#                      'ACTION_SQL'
#                      ],
#             conditions = ["DS_ID in (select DS_ID from data_sources where project_id in (21000, 44001, 45001))"])
########################################################################################################################
print('''
#Building new Tables
''')

usr1.batch_copy_tables(['ELN_FMLN_ING_ROLES_RATIO',
                        'ELN_FMLN_INGREDIENTS',
                        'ELN_FMLN_PROCEDURES',
                        'ELN_FMLN_PROCESS_PARAMS',
                        'ELN_FMLN_PROCESS_PARAMS_CHILD',
                        'ELN_FMLN_PROCESS_PARAMS_CHILD2',
                        'ELN_FMLN_RESULTS',
                        'ELN_FMLN_RESULTS_CHILD',
                        'ELN_FMLN_RESULTS_PARAMS',
                        'ELN_FMLN_SNAPSHOT_FORMULAE',
                        'ELN_FMLN_SNAPSHOT_PARAMS',
                        'ELN_FMLN_SNAPSHOT_PROCEDURES',
                        'ELN_FMLN_SNAPSHOTS',
                        'FMLN_PROCESS_PARAM_DICT',
                        'ELN_SAMPLE_RESULTS'
                        ],
                       ds3_userdata,
                       ds3_userdata2,
                       #'--verbose',
                       grant = ds3_appdata2.username
                       )
