import cx_Oracle
import datetime
import time
import re
import argparse
from config import ds3_userdata, ds3_appdata, pinpoint, gateway, ds3_userdata2


class OracleInterface:
    # Class to handle queries, inserts, merges and migrations into Oracle databases with simple syntax
    def __init__(self, schema):
        self.schema_name = schema.name
        self.username = schema.username
        self.password = schema.password
        self.dsn = schema.dsn
        self.encoding = schema.encoding
        self.connection = None
        self.cursor = None

    @staticmethod
    def date_filter(datain):
        if type(datain) == datetime.datetime:
            return '''to_date('{}', 'YYYY-MM-DD HH24:MI:SS')'''.format(str(datain))
        else:
            return datain

    @staticmethod
    def process_data(data):
        # This fuction just separates strings and numbers in insert statements from arguments
        return ["'{}'".format(d.replace("'", "''")) if type(d) == str and d[:7] != 'to_date' else str(d) for d in data]

    def connect(self):
        # Connect Oracle instance
        self.connection = cx_Oracle.connect(
                    self.username,
                    self.password,
                    self.dsn,
                    encoding=self.encoding)
        self.cursor = self.connection.cursor()

    def query(self, table, *args, **kwargs):
        # Simple Query method
        conditions = kwargs.get('conditions', '')  # conditions refers to WHERE clause
        headers = kwargs.get('headers', ['*'])  # No 'headers' kwarg defaults to SELECT *
        tabular = False  # Convenient dictionary syntax for query output
        if '--t' in args:
            tabular = True

        try:
            assert type(headers) == list
        except AssertionError:
            print('Headers must be provided as a list of strings')
            exit()
        if conditions:
            conditions = ' WHERE {}'.format(' and '.join(conditions))

        sql = "select {} from {}".format(', '.join([x for x in headers]), table) + conditions

        self.cursor.execute(sql)

        if tabular:
            if headers == ['*']:
                tab_out = {n: {header[0]: value for (header, value) in zip(self.cursor.description, row)} for n, row in
                           enumerate(self.cursor)}
            else:
                tab_out = {n: {header: value for (header, value) in zip(headers, row)}
                           for n, row in enumerate(self.cursor)}

            return tab_out
        else:
            return [value for value in self.cursor]  # Returns standard cx_Oracle query output

    def merge(self, table, data, headers, *args, **kwargs):
        #  Merge into tables with easy syntax
        verbose = False
        migration = False

        if '--verbose' in args:
            verbose = True

        if '--mig' in args:
            migration = True

        i_count = int(self.query(table, headers=['count(*)'])[0][0])  # intial row count
        data = self.process_data(data)  # get data into correct format for data types

        try:
            assert type(headers) == list
        except AssertionError:
            print('Headers must be provided as a list of strings')
            exit()
        conditions = kwargs.get('conditions', '')

        if conditions:
            conditions = ' WHERE {}'.format(' and '.join(conditions))

        try:
            assert type(headers) == list
        except AssertionError:
            print('Headers must be provided as a list of strings')
            exit()

        line1 = 'MERGE INTO {} T '.format(table)
        line2 = 'USING (SELECT {} from dual) VAL '.format(
            ', '.join('{} {}'.format(v, header) for v, header in zip([str(x) for x in data], headers)))
        line3 = 'ON ({}) '.format(' and '.join('T.{} = VAL.{}'.format(header, header) for header in headers))
        line4 = "WHEN NOT MATCHED THEN INSERT ({}) ".format(', '.join(headers))
        line5 = 'VALUES ({})'.format(', '.join(data))

        sql = ''.join([line1, line2, line3, line4, line5, conditions])
        if verbose:
            print(sql)
        try:
            self.cursor.execute(sql)
            self.connection.commit()
            if int(self.query(table, headers=['count(*)'])[0][0]) - i_count == 1:
                if migration and not verbose:
                    pass
                else:
                    print('1 row merged')
            elif int(self.query(table, headers=['count(*)'])[0][0]) - i_count == 0:
                if migration and not verbose:
                    pass
                else:
                    print('1 row merged')
        except cx_Oracle.Error as error:
            print(sql)
            print(error)

    def insert(self, table, data, headers, *args, **kwargs):
        #  Merge into tables with easy syntax
        verbose = False
        migration = False

        if '--verbose' in args:
            verbose = True

        if '--mig' in args:
            migration = True

        data = self.process_data(data)

        try:
            assert type(headers) == list
        except AssertionError:
            print('Headers must be provided as a list of strings')
            exit()
        conditions = kwargs.get('conditions', '')

        if conditions:
            conditions = ' WHERE {}'.format(' and '.join(conditions))

        try:
            assert type(headers) == list
        except AssertionError:
            print('Headers must be provided as a list of strings')
            exit()

        line1 = 'INSERT INTO {}'.format(table)
        line2 = "({}) ".format(', '.join(headers))
        line3 = 'VALUES ({})'.format(', '.join(data))

        sql = ''.join([line1, line2, line3, conditions])
        if verbose:
            print(sql)

        try:
            self.cursor.execute(sql)
            self.connection.commit()
            if migration and not verbose:
                pass
            else:
                print('1 row merged')
        except cx_Oracle.Error as error:
            print(error)
            print('migration halted')
            exit()

    def migrate(self, original_table, destination_schema, destination_table, *args, ** kwargs):
        # This function will move data from one table to another, including to a different database
        # WARNING! This is a MERGE migrate - it will not allow duplicate rows to be entered,
        # even if the exist in the source table
        # To ignore duplicates use flag --dup

        print()
        print('Migrating {} from {} to {}'.format(original_table, self.schema_name, destination_schema.name))
        print()

        dups = False
        verbose = False

        if '--dup' in args:
            dups = True

        if '--verbose' in args:
            verbose = '--verbose'

        conditions = kwargs.get('conditions', '')
        headers = kwargs.get('headers', ['*'])

        try:
            assert type(headers) == list
        except AssertionError:
            print('Headers must be provided as a list of strings')
            exit()

        if conditions:
            conditions = ' WHERE {}'.format(' and '.join(conditions))

        sql = "select {} from {}".format(', '.join([x for x in headers]), original_table) + conditions
        if verbose:
            print(sql)

        self.cursor.execute(sql)
        if headers == ['*']:
            tab_out = {n: {header[0]: value for (header, value) in zip(self.cursor.description, row) if value}
                       for n, row in enumerate(self.cursor)}
        else:
            tab_out = {n: {header: self.date_filter(value) for (header, value) in zip(headers, row) if value}
                       for n, row in enumerate(self.cursor)}
        if verbose:
            print(tab_out)

        destination_db = OracleInterface(destination_schema)
        destination_db.connect()


        counter = 0
        for row in tab_out:
            headers = [val for val in tab_out[row]]
            data = [tab_out[row][val] for val in tab_out[row]]
            if dups:
                destination_db.insert(destination_table, data, headers=headers)
            else:
                destination_db.merge(destination_table, data, headers, verbose, '--mig')
            counter += 1

        print('{} rows merged'.format(counter))

    def copy_table(self, original_table, original_schema, destination_schema, *args, ** kwargs):
        # This method will clone tables from one Schema to another

        grant = kwargs.get('grant', '')

        verbose = None
        if '--verbose' in args:
            verbose = '--verbose'

        sql = '''select dbms_metadata.get_ddl( 'TABLE', '{}', '{}' ) 
        from dual'''.format(original_table, original_schema.username.upper())

        self.cursor.execute(sql)
        print()

        gen = [line[0].read() for line in self.cursor][0]
        if verbose:
            print(gen)

        destination_db = OracleInterface(destination_schema)
        destination_db.connect()
        try:
            destination_db.cursor.execute(gen)
            print('Table {} Created in Schema {}'.format(original_table, destination_schema.name))

        except cx_Oracle.DatabaseError as x:
            print(x)
            if str(x)[:9] =='ORA-00955':
                print('...Skipping table')
            else:
                exit()

        if grant:
            grant_sql = '''
                GRANT ALL ON {} TO {}
                '''.format(original_table, grant.upper())
            destination_db.cursor.execute(grant_sql)
            print('Grant ALL to {} executed'.format(grant))



    def batch_copy_tables(self, tables, original_schema, destination_schema, *args, ** kwargs):
        # This method can be used to clone multiple tables from one schema to another, as long as they share the same
        # origin and destination schema

        grant = kwargs.get('grant', '')

        verbose = None
        if '--verbose' in args:
            verbose = '--verbose'

        for table in tables:
            self.copy_table(table, original_schema, destination_schema, verbose, grant = grant)

    def sandbox(self, sql, *args):
        verbose = None
        if '--verbose' in args:
            verbose = '--verbose'
        # Send any SQL - use at own risk
        if verbose:
            print(sql)
        self.cursor.execute(sql)
        return [value for value in self.cursor]


########################################################################################################################

# I have produced a convenient class to handle some simple Oracle interactions.
# To use - set up config.py to include any schemas / databases etc. that you would like to interact with.
#        - Import these schemas from config.py

### --EXAMPLE--

#if __name__ == '__main__':
    #db = OracleInterface(ds3_userdata) # Instantiate Oracleinterface instance for your schema
    #db.connect()  # connect to database

    # example query: arguments = table_name. *args = --t flag for tabular output. **kwargs = headers:specify headers,
    # conditions: WHERE clause
    #output = db.query('MIGRATION_TEST_TABLE', '--t', headers=['COLUMN1', 'COLUMN2', 'COLUMN3', 'COLUMN4'],
                      #conditions = ["COLUMN1 = 'q'", "COLUMN4 = 1"])

    #for row in output:
        #print(row, output[row])

    # example INSERT: arguments = table_name, data (to insert), headers (in same order as data)

   #db.insert('MIGRATION_TEST_TABLE',
   #          ['q','b', 'c', 1],
   #          ['COLUMN1', 'COLUMN2', 'COLUMN3', 'COLUMN4'])

   ## example MERGE: arguments = table_name, data (to insert), headers (in same order as data)

   #db.merge('MIGRATION_TEST_TABLE',
   #         ['z', 'b', 'c', 1],
   #         ['COLUMN1', 'COLUMN2', 'COLUMN3', 'COLUMN4' ])

   ## example migrate: arguments = original_table_name, destination_schema, detination_table_name data (to insert),
   ##*args --dup allows for duplicates to be inserted- careful
   ##** kwargs = headers:specify headers, conditions: WHERE clause for initial query

   #db.migrate('MIGRATION_TEST_TABLE', ds3_userdata2, 'MIGRATION_TEST_TABLE', conditions = ["COLUMN1 = 'x'", "COLUMN4 = 1"])






### Command line stuff - unfinished
#    parser = argparse.ArgumentParser(description='Please provide schema and sql')
#    parser.add_argument('schema', help='schema to query')
#    parser.add_argument('sql', help='sql statement')
#    ags = parser.parse_args()
#    schemas = {'ds3_userdata': ds3_userdata,
#               'ds3_apprdata': ds3_appdata,
#               'pinpoint': pinpoint,
#               'gateway': gateway,
#               }
#    db = OracleInterface(schemas[ags.schema])
#    db.connect()
#    output = db.sandbox(ags.sql)
#    print(output)
