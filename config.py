class Schema:
    def __init__(self, username, password, dsn, port, encoding, name):
        self.username = username
        self.password = password
        self.dsn = dsn
        self.port = port
        self.encoding = encoding
        self.name = name


ds3_userdata = Schema('ds3_userdata', 'ds3_userdata', 'localhost/XEPDB1', 1521, 'UTF-8', 'ds3_userdata')
ds3_appdata = Schema('ds3_appdata', 'ds3_appdata', 'localhost/XEPDB1', 1521, 'UTF-8', 'ds3_appdata')
pinpoint = Schema('c$pinpoint', 'pinpoint', 'localhost/XEPDB1', 1521, 'UTF-8', 'Pinpoint')
gateway = Schema('gateway', 'dataowner', 'localhost/XEPDB1', 1521, 'UTF-8', 'Gateway')

ds3_userdata2 = Schema('ds3_userdata', 'ds3_userdata', 'localhost/XEPDB2', 1521, 'UTF-8', 'ds3_userdata-2')
ds3_appdata2 = Schema('ds3_appdata', 'ds3_appdata', 'localhost/XEPDB2', 1521, 'UTF-8', 'ds3_appdata-2')
pinpoint2 = Schema('c$pinpoint', 'pinpoint', 'localhost/XEPDB2', 1521, 'UTF-8', 'Pinpoint-2')
gateway2 = Schema('gateway', 'dataowner', 'localhost/XEPDB2', 1521, 'UTF-8', 'Gateway-2')